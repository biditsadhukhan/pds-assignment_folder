---
title: "PDS Assignment Bidit,Nirmalya"
author: "Bidit Sadhukhan"
date: "2022-12-27"
output: 
  html_document: 
    toc: yes
    highlight: zenburn
    theme: readable
    number_sections: yes
    keep_md: yes
    df_print: kable
editor_options: 
  chunk_output_type: console
---

# Telecom Churn Data Analysis

> What is Customer Churn?

Customer attrition, also known as customer churn, customer turnover, or customer defection, is the loss of clients or customers.

Banks, telephone service companies, Internet service providers, pay TV companies, insurance firms, and alarm monitoring services, often use customer attrition analysis and customer attrition rates as one of their key business metrics (along with cash flow, EBITDA, etc.) because the cost of retaining an existing customer is far less than acquiring a new one. Companies from these sectors often have customer service branches which attempt to win back defecting clients, because recovered long-term customers can be worth much more to a company than newly recruited clients.

Companies usually make a distinction between voluntary churn and involuntary churn. Voluntary churn occurs due to a decision by the customer to switch to another company or service provider, involuntary churn occurs due to circumstances such as a customer's relocation to a long-term care facility, death, or the relocation to a distant location. In most applications, involuntary reasons for churn are excluded from the analytical models. Analysts tend to concentrate on voluntary churn, because it typically occurs due to factors of the company-customer relationship which companies control, such as how billing interactions are handled or how after-sales help is provided. [Learn More](https://en.wikipedia.org/wiki/Customer_attrition)

## About Data

*Data Cleaning Process*



```r
library(tidyverse)
library(dplyr)
library(readxl)
library(ggplot2)
library(moments)
library(gridExtra)
library(plotrix)
library(viridis)
library(ggpubr)
library(hrbrthemes)
library(forcats)
library(magrittr)
library(treemap)
library(ggridges)
```


**Reading Data From File**

```r
path_file <- "Dataset\\tourn_1_calibration_csv raw_data.csv"
```
a. Reading the csv file into the dataframe *churn_data_raw* and then trying to understand the basic structure and dimension of the dataframe using the str and the dim function in R


```r
# Importing the Dataset
churn_data_raw <- read.csv(path_file,header = T,blank.lines.skip = T,stringsAsFactors = TRUE)
# Structure of the dataset
str(head(churn_data_raw[1:10],n=2))    
```

```
## 'data.frame':	2 obs. of  10 variables:
##  $ rev_Mean   : num  24 57.5
##  $ mou_Mean   : num  219 483
##  $ totmrc_Mean: num  22.5 37.4
##  $ da_Mean    : num  0.247 0.247
##  $ ovrmou_Mean: num  0 22.8
##  $ ovrrev_Mean: num  0 9.1
##  $ vceovr_Mean: num  0 9.1
##  $ datovr_Mean: num  0 0
##  $ roam_Mean  : num  0 0
##  $ rev_Range  : num  26 153
```

```r
# Dimension
dim(churn_data_raw)
```

```
## [1] 100000    173
```


```r
# Converting the empty spaces into NA in dataframe
churn_data_raw[churn_data_raw == ""] <- NA 

# Function for counting the missing values in the coloumns
 missing_data_count <- function(x){
   
   sum(is.na(x))
 } 
 #Filtering the dataset based on the rows condition that there is no NA values in rows
churn_data_raw2 <- churn_data_raw[,(sapply(churn_data_raw,missing_data_count)>=50000)==F]
churn_data_filter2 <- na.omit(churn_data_raw2)   
# Filtered Data
dim(churn_data_filter2)
```

```
## [1] 26705   151
```



```r
#numeric variables
selected_numeric_variable <- c(2,8,9,50,41,42)
sapply(churn_data_filter[,selected_numeric_variable],summary)
```

```
##            da_Mean vceovr_Range datovr_Range owylis_vce_Range unan_vce_Range
## Min.     0.0000000      0.00000    0.0000000          0.00000        0.00000
## 1st Qu.  0.0000000      0.00000    0.0000000          2.00000        3.00000
## Median   0.0000000      0.00000    0.0000000          7.00000       10.00000
## Mean     0.8135603     26.38048    0.5962322         13.11942       17.97173
## 3rd Qu.  0.7425000     31.20000    0.0000000         16.00000       22.00000
## Max.    72.7650000   1215.55000  409.5000000        554.00000     1010.00000
##         unan_dat_Range
## Min.        0.00000000
## 1st Qu.     0.00000000
## Median      0.00000000
## Mean        0.05931474
## 3rd Qu.     0.00000000
## Max.       83.00000000
```

```r
sapply(churn_data_filter[,selected_numeric_variable],skewness)
```

```
##          da_Mean     vceovr_Range     datovr_Range owylis_vce_Range 
##         7.852205         4.931640        31.984711         5.811296 
##   unan_vce_Range   unan_dat_Range 
##         8.173447        64.177128
```

```r
#categorical variables
selected_categorical_variable <- churn_data_filter[,is.factor(churn_data_filter)==T]
#cross-tabulation table
with(churn_data_filter,table(churn,actvsubs))
```

```
##      actvsubs
## churn    0    1    2    3    4    5    6    9
##     0   12 9036 4158  755  178   52    2    0
##     1    6 7791 3910  615  158   30    1    1
```

```r
with(churn_data_filter,table(churn,area))
```

```
##      area
## churn      ATLANTIC SOUTH AREA CALIFORNIA NORTH AREA CENTRAL/SOUTH TEXAS AREA
##     0    0                 650                   444                      757
##     1    0                 564                   489                      562
##      area
## churn CHICAGO AREA DALLAS AREA DC/MARYLAND/VIRGINIA AREA GREAT LAKES AREA
##     0          660         954                       976              989
##     1          576         832                       761              754
##      area
## churn HOUSTON AREA LOS ANGELES AREA MIDWEST AREA NEW ENGLAND AREA
##     0          790              664         1169              616
##     1          657              637          850              595
##      area
## churn NEW YORK CITY AREA NORTH FLORIDA AREA NORTHWEST/ROCKY MOUNTAIN AREA
##     0               1187                759                           486
##     1               1173                746                           582
##      area
## churn OHIO AREA PHILADELPHIA AREA SOUTH FLORIDA AREA SOUTHWEST AREA
##     0       959               298                554            739
##     1       732               293                578            739
##      area
## churn TENNESSEE AREA
##     0            542
##     1            392
```

```r
with(churn_data_filter,table(churn,marital))
```

```
##      marital
## churn         A    B    M    S    U
##     0    0 1187  776 7583 2799 1848
##     1    0 1034  698 6600 2382 1798
```

```r
with(churn_data_filter,table(churn,models))
```

```
##      models
## churn    1    2    3    4    5    6    7    8    9   10   11
##     0 8467 3873 1257  392  127   51   17    7    2    0    0
##     1 7915 3111  997  311  107   47   11    7    3    2    1
```

```r
with(churn_data_filter,table(churn,uniqsubs))
```

```
##      uniqsubs
## churn    1    2    3    4    5    6    7    8    9   10   11   13
##     0 8207 4364 1058  381  127   40    9    4    2    1    0    0
##     1 6818 4169  971  358  126   43   15    5    4    1    1    1
```

```r
with(churn_data_filter,table(uniqsubs,actvsubs,churn))
```

```
## , , churn = 0
## 
##         actvsubs
## uniqsubs    0    1    2    3    4    5    6    9
##       1     8 8199    0    0    0    0    0    0
##       2     0  710 3654    0    0    0    0    0
##       3     2   97  360  599    0    0    0    0
##       4     1   23  130   98  129    0    0    0
##       5     1    4   10   38   39   35    0    0
##       6     0    2    3   16    7   10    2    0
##       7     0    0    1    1    2    5    0    0
##       8     0    0    0    3    1    0    0    0
##       9     0    1    0    0    0    1    0    0
##       10    0    0    0    0    0    1    0    0
##       11    0    0    0    0    0    0    0    0
##       13    0    0    0    0    0    0    0    0
## 
## , , churn = 1
## 
##         actvsubs
## uniqsubs    0    1    2    3    4    5    6    9
##       1     3 6815    0    0    0    0    0    0
##       2     2  798 3369    0    0    0    0    0
##       3     1  135  372  463    0    0    0    0
##       4     0   34  130   91  103    0    0    0
##       5     0    7   28   39   31   21    0    0
##       6     0    2    8   16   11    6    0    0
##       7     0    0    2    3    8    1    1    0
##       8     0    0    1    2    2    0    0    0
##       9     0    0    0    0    2    1    0    1
##       10    0    0    0    0    1    0    0    0
##       11    0    0    0    1    0    0    0    0
##       13    0    0    0    0    0    1    0    0
```


```r
knitr::opts_chunk$set(warning = F)

levels(churn_data_filter2$marital)<- c("NA","Inf-Single","Inf-Married","Married","Single","Unknown")
f1 = churn_data_filter2 %>%
  mutate(text = fct_reorder(marital, adjmou))%>% 
  ggplot( aes(x=marital, y=adjmou, fill=marital)) +
    geom_violin(width=2.1,size=0.2) +
    scale_fill_viridis(discrete=TRUE) +
  scale_color_viridis(discrete=TRUE)+
    theme_ipsum(plot_title_size = 15) +
    theme(
    ) +
    coord_flip() +
    xlab("Marital Status") +
    ylab("Monthly revenue")+labs(title="Monthly Revenue & Marital Status",caption = "Data Source:Churn Data",tag = "Figure-1",fill="Marital Status",position="dodge")

f2 = churn_data_filter2 %>%
  mutate(text = fct_reorder(marital,mou_Mean)) %>%
   ggplot( aes(x=marital, y=mou_Mean, fill=marital)) +
    geom_violin(width=2.1, size=0.2) +
    scale_fill_viridis(discrete=TRUE) +
  scale_color_viridis(discrete=TRUE)+
    theme_ipsum(plot_title_size = 12) +
    theme(legend.position = "none"
    ) +
    coord_flip() +
    xlab("Marital Status") +
    ylab("Monthly mintues of use(mins)")+labs(title="Mean Monthly Usage & Marital Status",caption = "Data Source:Churn Data",tag = "Figure-2",fill="Marital Status")

f3 = churn_data_filter2 %>%
  mutate(text = fct_reorder(marital,rev_Mean)) %>%
   ggplot( aes(x=marital, y=rev_Mean, fill=marital)) +
    geom_violin(width=2.1, size=0.2) +
    scale_fill_viridis(discrete=TRUE) +
  scale_color_viridis(discrete=TRUE)+
    theme_ipsum(plot_title_size = 15) +
    theme(legend.position = "none"
    ) +
    coord_flip() +
    xlab("Marital Status") +
    ylab("Total Revenue")+labs(title="Total Revenue & Marital Status",caption = "Data Source:Churn Data",tag = "Figure-3",fill="Marital Status")
# creating a temp dataframe
df <- data.frame(table(churn_data_filter2$marital))

f01<- df %>%
  arrange(Freq)%>%
  mutate(Var1=factor(Var1, levels=Var1)) %>%
  ggplot( aes(x=Var1, y= Freq)) +
    geom_segment( aes(x=Var1 ,xend=Var1, y=0, yend=Freq), color="grey") +
    geom_point(size=3, color="#69b3a2") +
    coord_flip() +
    theme_minimal() +
    theme(
      panel.grid.minor.y = element_blank(),
      panel.grid.major.y = element_blank(),
      legend.position="none"
    ) +
    xlab("Marital Status") +
    ylab("Population (M)")
#############################################


figure1 <- ggarrange(f01,
  f1, f2,f3 ,ncol=2,nrow=2    ,widths = c(0.5,1,1,1)     # First row with line plot
  # Second row with box and dot plots
     # Label of the line plot
  ) 
annotate_figure(figure1, top = text_grob("Visualizing revenue and total minutes of usage based on \n Marital Status",
                  color = "red", face = "bold", size = 14),
  bottom = text_grob("Data source:Churn Data", color = "black",
                     hjust = 1, x = 1, face = "italic", size = 10),
  
  )
```

![](Assignment-1,-Churnds-selection_files/figure-html/plotting monthly revenue,calls placed and mean of monthly minutes of use with marital status-1.png)<!-- -->


```r
knitr::opts_chunk$set(warning = F)
churn_data_filter2 %>%
  mutate(text = fct_reorder(marital, rev_Mean)) %>%
  ggplot( aes(x=rev_Mean, fill=marital)) +
    geom_density(alpha=0.6) +
    scale_fill_viridis(discrete=TRUE) +
    scale_color_viridis(discrete=TRUE) +
    theme_minimal() +
    theme( panel.spacing = unit(0.1, "lines"),
      strip.text.x = element_text(size = 8)
     
    ) +xlim(100,600)+
    xlab("Monthly Revenue") +
    ylab("Frequency Density (%)") +
    labs(title="Monthly Revenue & Marital Status",caption = "Data Source:Churn Data",tag = "Figure-2",fill="Marital Status")
```

![](Assignment-1,-Churnds-selection_files/figure-html/part two marital status-1.png)<!-- -->

```r
churn_data_filter2 %>%
  mutate(text = fct_reorder(marital, totrev)) %>%
  ggplot( aes(x=totrev, fill=marital)) +
    geom_density(alpha=0.6) +
    scale_fill_viridis(discrete=TRUE) +
    scale_color_viridis(discrete=TRUE) +
    theme_minimal() +
    theme( panel.spacing = unit(0.1, "lines"),
      strip.text.x = element_text(size = 8)
     
    ) +
    xlab("Total Revenue") +
    ylab("Frequency Density (%)") +
    labs(title="Total Revenue & Marital Status",caption = "Data Source:Churn Data",tag = "Figure-3",fill="Marital Status")
```

![](Assignment-1,-Churnds-selection_files/figure-html/part two marital status-2.png)<!-- -->

```r
df2 <- data.frame(table(churn_data_filter2$area,churn_data_filter2$churn))

df_2<- data.frame(df2[df2$Var2==0,][,3]-df2[df2$Var2==1,][,3],df2[df2$Var2==0,][,1])
colnames(df_2)<- c("var1","var2")
df_2$label<- paste(df_2$var2,df_2$var1,sep="-")
df_2$var3<- abs(df_2$var1)

as.factor(df2$Var2)
```

```
##  [1] 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
## [39] 1 1
## Levels: 0 1
```

```r
treemap(df_2,
            index="label",
            vSize="var3",
        vColor = "var1",
            type="value",
        algorithm="pivotSize",
        sortID="-size",
        title="Area Wise Churn", #Customize your title
        title.legend = "Churn",
        fontsize.title = 14 #Change the font size of the title

            )
```

![](Assignment-1,-Churnds-selection_files/figure-html/area based plot of the total revenue-1.png)<!-- -->

```r
churn_data_filter2 %>%
  ggplot( aes(x=attempt_Mean, y=blck_vce_Mean, group=area,col=area)) +
    geom_line() +
    scale_color_viridis(discrete = TRUE) +
    ggtitle("Area- wise distribution of attempted and blocked calls") +
    theme_ipsum()+theme(legend.position="none",
      panel.spacing = unit(0, "lines"),
      strip.text.x = element_text(size = 8),
      plot.title = element_text(size=13)) +ylim(0,300)+xlab("Attempted Mean Calls")+
    ylab("Blocked Calls")+facet_wrap(~area, scale="free_y")
```

![](Assignment-1,-Churnds-selection_files/figure-html/area based plot of the total revenue-2.png)<!-- -->

```r
churn_data_filter2$income<- factor(churn_data_filter2$income,labels =c("<15k","15k-20k","20k-30k","30k-40k","40k-50k","50k-75k","75k-100k","100k-125k",">125k"),exclude = NA )
df3 <- data.frame(table(churn_data_filter2$income,churn_data_filter2$churn))

df_3<- data.frame(df3[df3$Var2==0,][,3]-df3[df3$Var2==1,][,3],df3[df3$Var2==0,][,1])
colnames(df_3)<- c("var1","var2")
df_3$label<- paste(df_3$var2,df_3$var1,sep="  :")
df_3$var3<- abs(df_3$var1)

as.factor(df3$Var2)
```

```
##  [1] 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1
## Levels: 0 1
```

```r
treemap(df_3,
            index="label",
            vSize="var3",
        vColor = "var1",
            type="value",
        algorithm="pivotSize",
        sortID="-size",
        title="Income Wise Churn", #Customize your title
        title.legend = "Churn",
        fontsize.title = 14 #Change the font size of the title

            )
```

![](Assignment-1,-Churnds-selection_files/figure-html/income level with churn-1.png)<!-- -->

```r
churn_data_filter2 %>%
  ggplot( aes(x=attempt_Mean, y=blck_vce_Mean, group=income,col=income)) +
    geom_line() +
    scale_color_viridis(discrete = TRUE) +
    ggtitle("Income-wise Distribution of  Blocked and Attempted Calls") +
    theme_ipsum()+theme(legend.position="none",
      panel.spacing = unit(0, "lines"),
      strip.text.x = element_text(size = 8),
      plot.title = element_text(size=13))+ylim(0,300) +xlab("Attempted Mean Calls")+
    ylab("Blocked Calls")+facet_wrap(~income, scale="free_y")
```

![](Assignment-1,-Churnds-selection_files/figure-html/income level with churn-2.png)<!-- -->

```r
churn_data_filter2$ethnic<- factor(churn_data_filter2$ethnic,exclude = NA )
df4 <- data.frame(table(churn_data_filter2$ethnic,churn_data_filter2$churn))

df_4<- data.frame(df4[df4$Var2==0,][,3]-df4[df4$Var2==1,][,3],df4[df4$Var2==0,][,1])
colnames(df_4)<- c("var1","var2")
df_4$label<- paste(df_4$var2,df_4$var1,sep="  :")
df_4$var3<- abs(df_4$var1)

as.factor(df4$Var2)
```

```
##  [1] 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
## Levels: 0 1
```

```r
treemap(df_4,
            index="label",
            vSize="var3",
        vColor = "var1",
            type="value",
        algorithm="pivotSize",
        sortID="-size",
        title="Ethnicity Wise Churn", #Customize your title
        title.legend = "Churn",
        fontsize.title = 14 #Change the font size of the title

            )
```

![](Assignment-1,-Churnds-selection_files/figure-html/ethnicity vs churn-1.png)<!-- -->

```r
churn_data_filter2 %>%
  ggplot( aes(x=attempt_Mean, y=blck_vce_Mean, group=ethnic,col=ethnic)) +
    geom_line() +
    scale_color_viridis(discrete = TRUE) +
    ggtitle("Ethnicity-wise distribution of blocked and attempted calls") +
    theme_ipsum()+theme(legend.position="none",
      panel.spacing = unit(0, "lines"),
      strip.text.x = element_text(size = 8),
      plot.title = element_text(size=13)) +ylim(0,200)+xlab("Attempted Mean Calls")+
    ylab("Blocked Calls")+facet_wrap(~ethnic, scale="free_y")
```

```
## `geom_line()`: Each group consists of only one observation.
## ℹ Do you need to adjust the group aesthetic?
```

![](Assignment-1,-Churnds-selection_files/figure-html/ethnicity vs churn-2.png)<!-- -->

```r
churn_data_filter2$hnd_webcap<- factor(churn_data_filter2$hnd_webcap,labels =c("Web Capable","Web Capable Mini Browser"),exclude = NA )
df5 <- data.frame(table(churn_data_filter2$hnd_webcap,churn_data_filter2$churn))

df_5<- data.frame(df5[df5$Var2==0,][,3]-df5[df5$Var2==1,][,3],df5[df5$Var2==0,][,1])
colnames(df_5)<- c("var1","var2")
df_5$label<- paste(df_5$var2,df_5$var1,sep="  :")
df_5$var3<- abs(df_5$var1)

as.factor(df5$Var2)
```

```
## [1] 0 0 1 1
## Levels: 0 1
```

```r
treemap(df_5,
            index="label",
            vSize="var3",
        vColor = "var1",
            type="value",
        algorithm="pivotSize",
        sortID="-size",
        title="Handset Web capability Wise Churn", #Customize your title
        title.legend = "Churn",
        fontsize.title = 14 #Change the font size of the title

            )
```

![](Assignment-1,-Churnds-selection_files/figure-html/handset webcapability vs churn-1.png)<!-- -->



```r
drop_grandmean <- round(mean(churn_data_filter2$drop_blk_Mean),1)
  ggplot(churn_data_filter2,aes(x=as.factor(churn), y=drop_blk_Mean,fill=as.factor(churn)))+geom_boxplot(alpha=0.3)+theme(legend.position = "none")+scale_fill_brewer(palette = "Dark2")+ylab("Phone Usage Time(in hour)")+geom_hline(aes(yintercept=drop_grandmean),colour="#6666CC",lwd=1)+geom_text(aes(0,drop_grandmean,label= paste(" Grand Average",drop_grandmean,sep = ":"),vjust=1,hjust=0),colour="#6666FF")+stat_summary(fun = mean,geom="point",shape=20,size=5,color="red",fill="red")+scale_x_discrete(labels=c("Yes","NO"))+ylim(0,75)
```

![](Assignment-1,-Churnds-selection_files/figure-html/Boxplot of 6 months usage with churn-1.png)<!-- -->


```r
churn_data_filter2 <- within(churn_data_filter2, {   
  age1_1 <- NA # need to initialize variable
  age1_1[age1 <= 18] <- "<18"
  age1_1[age1 >18 & age1< 24] <- "18-24"
  age1_1[age1 >=24 & age1< 30 ] <- "24-30"
  age1_1[age1 >=30 & age1< 40] <- "30-40"
  age1_1[age1 >=40 & age1< 50] <- "40-50"
  age1_1[age1 >=50 & age1< 60] <- "50-60"
  age1_1[age1 >=60 & age1< 70] <- "60-70"
  age1_1[age1 >=70 & age1< 80] <- "70-80"
  age1_1[age1 >= 80] <- ">80"
   } )
churn_data_filter2 %>%
  ggplot( aes(x=totcalls, y=totmou, group=as.factor(age1_1),col=as.factor(age1_1))) +
    geom_line() +
    scale_color_viridis(discrete = TRUE) +
    ggtitle("Age-wise distribution of Total number of calls and Total minutes of usage") +
    theme_ipsum()+theme(legend.position="none",
      panel.spacing = unit(0, "lines"),
      strip.text.x = element_text(size = 8),
      plot.title = element_text(size=13)) +ylim(0,125000)+xlab("Total Number of call")+
    ylab("Total Minutes of Usage")+facet_wrap(~as.factor(age1_1), scale="free_y")
```

![](Assignment-1,-Churnds-selection_files/figure-html/age1-1.png)<!-- -->


```r
# Continous variable plots
ggplot( churn_data_filter,aes(x=totcalls,y=marital)) +
    geom_violin( binwidth=200, fill="#69b3a2", color="#e9ecef", alpha=0.9) +
    ggtitle("Bin size = 3") +
    theme_classic() +
    theme(
      plot.title = element_text(size=15)
    )
```

![](Assignment-1,-Churnds-selection_files/figure-html/unnamed-chunk-1-1.png)<!-- -->

```r
ggplot(churn_data_filter)+geom_histogram(mapping=aes(x=totcalls),binwidth = 50,fill="#69b3a2")
```

![](Assignment-1,-Churnds-selection_files/figure-html/unnamed-chunk-1-2.png)<!-- -->


```r
ggplot(churn_data_filter,aes(x=as.factor(actvsubs)))+
geom_bar(position="dodge",na.rm = T, stat="count",fill="#FF6666")+theme_minimal()+xlab("Active Subscribers")+ylab("Count")
```

![](Assignment-1,-Churnds-selection_files/figure-html/unnamed-chunk-2-1.png)<!-- -->




```r
#select_if(churn_data_filter,is.factor)

p1 <- ggplot(churn_data_filter, aes(x=marital)) + ggtitle("Marital Status") + xlab("Marriage Status") +
  geom_bar(aes(y = 100*(..count..)/sum(..count..)), width = 0.5) + ylab("Percentage") + coord_flip() + theme_minimal()+scale_x_discrete(labels=c("Single","Married","Unmarried","Inferred Single","Inferred Married"))+xlab("Marriage status")


p2 <- ggplot(churn_data_filter, aes(x=hnd_webcap)) + ggtitle("Handset web Capability") + xlab("web Capability") + 
  geom_bar(aes(y = 100*(..count..)/sum(..count..)), width = 0.5) + ylab("Percentage") + coord_flip() + theme_minimal()

p3 <- ggplot(churn_data_filter, aes(x=dwlltype)) + ggtitle("Dwelling Type") + xlab("Dwelling") + 
  geom_bar(aes(y = 100*(..count..)/sum(..count..)), width = 0.5) + ylab("Percentage") + coord_flip() + theme_minimal()


p4 <- ggplot(churn_data_filter, aes(x=dualband)) + ggtitle("Dualband") + xlab("Dualbands") +
  geom_bar(aes(y = 100*(..count..)/sum(..count..)), width = 0.5) + ylab("Percentage") + coord_flip() + theme_minimal()
grid.arrange(p1, p2, p3, p4, ncol=2)
```

![](Assignment-1,-Churnds-selection_files/figure-html/unnamed-chunk-3-1.png)<!-- -->

```r
ggplot(data = churn_data_filter) + geom_line(aes(x=months, y=as.factor(marital)))
```

![](Assignment-1,-Churnds-selection_files/figure-html/unnamed-chunk-3-2.png)<!-- -->

```r
ggplot(churn_data_filter,aes(x = months, color=as.factor(churn))) + geom_freqpoly(size=2)+labs(color="Churn Status")
```

```
## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
```

![](Assignment-1,-Churnds-selection_files/figure-html/unnamed-chunk-3-3.png)<!-- -->

```r
ggplot(churn_data_filter,aes(x = mou_Range, color=as.factor(churn))) + geom_freqpoly(size=2)+labs(color="Churn Status")
```

```
## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
```

![](Assignment-1,-Churnds-selection_files/figure-html/unnamed-chunk-3-4.png)<!-- -->

```r
  # Limits of the plot = very important. The negative value controls the size of the inner circle, the positive one is useful to add size over each bar

  
  # Custom the theme: no axis title and no cartesian grid
```





```
## Time difference of 37.98123 secs
```


```r
sessionInfo()
```

```
## R version 4.2.2 (2022-10-31 ucrt)
## Platform: x86_64-w64-mingw32/x64 (64-bit)
## Running under: Windows 10 x64 (build 22621)
## 
## Matrix products: default
## 
## locale:
## [1] LC_COLLATE=English_India.utf8  LC_CTYPE=English_India.utf8   
## [3] LC_MONETARY=English_India.utf8 LC_NUMERIC=C                  
## [5] LC_TIME=English_India.utf8    
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
##  [1] ggridges_0.5.4    treemap_2.4-3     magrittr_2.0.3    hrbrthemes_0.8.0 
##  [5] ggpubr_0.5.0      viridis_0.6.2     viridisLite_0.4.1 plotrix_3.8-2    
##  [9] gridExtra_2.3     moments_0.14.1    readxl_1.4.1      forcats_0.5.2    
## [13] stringr_1.5.0     dplyr_1.0.10      purrr_1.0.0       readr_2.1.3      
## [17] tidyr_1.2.1       tibble_3.1.8      ggplot2_3.4.0     tidyverse_1.3.2  
## 
## loaded via a namespace (and not attached):
##  [1] fs_1.5.2            lubridate_1.9.0     RColorBrewer_1.1-3 
##  [4] httr_1.4.4          tools_4.2.2         backports_1.4.1    
##  [7] bslib_0.4.2         utf8_1.2.2          R6_2.5.1           
## [10] DBI_1.1.3           colorspace_2.0-3    withr_2.5.0        
## [13] tidyselect_1.2.0    compiler_4.2.2      extrafontdb_1.0    
## [16] cli_3.5.0           rvest_1.0.3         xml2_1.3.3         
## [19] labeling_0.4.2      sass_0.4.4          scales_1.2.1       
## [22] systemfonts_1.0.4   digest_0.6.31       rmarkdown_2.19     
## [25] pkgconfig_2.0.3     htmltools_0.5.4     extrafont_0.18     
## [28] highr_0.10          dbplyr_2.2.1        fastmap_1.1.0      
## [31] rlang_1.0.6         rstudioapi_0.14     shiny_1.7.4        
## [34] farver_2.1.1        jquerylib_0.1.4     generics_0.1.3     
## [37] jsonlite_1.8.4      car_3.1-1           googlesheets4_1.0.1
## [40] Rcpp_1.0.9          munsell_0.5.0       fansi_1.0.3        
## [43] abind_1.4-5         gdtools_0.2.4       lifecycle_1.0.3    
## [46] stringi_1.7.8       yaml_2.3.6          carData_3.0-5      
## [49] grid_4.2.2          promises_1.2.0.1    crayon_1.5.2       
## [52] cowplot_1.1.1       haven_2.5.1         hms_1.1.2          
## [55] knitr_1.41          pillar_1.8.1        igraph_1.3.5       
## [58] ggsignif_0.6.4      reprex_2.0.2        glue_1.6.2         
## [61] evaluate_0.19       data.table_1.14.6   modelr_0.1.10      
## [64] vctrs_0.5.1         tzdb_0.3.0          httpuv_1.6.7       
## [67] Rttf2pt1_1.3.11     cellranger_1.1.0    gtable_0.3.1       
## [70] assertthat_0.2.1    cachem_1.0.6        xfun_0.36          
## [73] gridBase_0.4-7      mime_0.12           xtable_1.8-4       
## [76] broom_1.0.2         rstatix_0.7.1       later_1.3.0        
## [79] googledrive_2.0.0   gargle_1.2.1        timechange_0.1.1   
## [82] ellipsis_0.3.2
```
